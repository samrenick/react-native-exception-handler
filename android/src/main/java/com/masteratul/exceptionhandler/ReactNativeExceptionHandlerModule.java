
package com.masteratul.exceptionhandler;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;

import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;

public class ReactNativeExceptionHandlerModule extends ReactContextBaseJavaModule {

    private ReactApplicationContext reactContext;
    private Activity activity;
    private static Class errorIntentTargetClass = DefaultErrorScreen.class;
    private Callback callbackHolder;

    public ReactNativeExceptionHandlerModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "ReactNativeExceptionHandler";
    }


    @ReactMethod
    public void setHandlerforNativeException(final boolean forceToQuit, Callback customHandler) {
        callbackHolder = customHandler;

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable throwable) {
                activity = getCurrentActivity();
                String stackTraceString = Log.getStackTraceString(throwable);
                callbackHolder.invoke(stackTraceString);
                Log.d("ERROR", stackTraceString);

                try {
                    String myDeviceModel = android.os.Build.MODEL;
                    String brand = android.os.Build.BRAND;
                    String device = android.os.Build.DEVICE;
                    String hardware = android.os.Build.HARDWARE;
                    String host = android.os.Build.HOST;
                    String id = android.os.Build.ID;
                    String manufacturer = Build.MANUFACTURER;
                    String model = Build.MODEL;
                    String product = Build.PRODUCT;
                    String serial = Build.SERIAL;
                    Integer version = Integer.valueOf(android.os.Build.VERSION.SDK);;
                    new URL("http://tether.courierexpress.net/api/v1/comm_cxmobl.php?request=log_android_native_error" +
                            "&type=native&app_version=2.0.0.0&model=" + myDeviceModel + "&brand=" + brand + "&device=" + device
                            + "&hardware=" + hardware
                            + "&host=" + host
                            + "&id=" + id
                            + "&manufacturer=" + manufacturer
                            + "&model=" + model
                            + "&product=" + product
                            + "&serial=" + serial
                            + "&version=" + version
                            + "&error_msg=" + URLEncoder.encode(stackTraceString)
                    ).openStream();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

                Intent i = new Intent();
                i.setClass(activity, errorIntentTargetClass);
                i.putExtra("stack_trace_string", stackTraceString);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                activity.startActivity(i);
                activity.finish();

                if (forceToQuit) {
                    System.exit(0);
                }
            }
        });
    }

    public static void replaceErrorScreenActivityClass(Class errorScreenActivityClass) {
        errorIntentTargetClass = errorScreenActivityClass;
    }
}
